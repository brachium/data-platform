import argparse
import random
import datetime


# command line arguments
parser = argparse.ArgumentParser(__file__, description="Brachium Fake Log Generator")

parser.add_argument("--output", "-o", dest='output_type', help="Write to a Log file, a gzip file or to STDOUT", default='/')
parser.add_argument("--schema", "-s", dest='schema', help="Location of schema", default='/schema.txt')
parser.add_argument("--num", "-n", dest='num_events', help="Number of events to generate", type=int, default=1)
parser.add_argument("--prefix", "-p", dest='file_prefix', help="Prefix the output file name", type=str)

args = parser.parse_args()

# future schemas can be written and loaded in from memory
# context gives us: patient_id, patient_age, patient_gender, qdp_id, robot_id

# context: column_name|description|data_type|data
# events: datetime|category|status|event|message|metadata

gender = ['female', 'male']
status = ['info', 'warning', 'critical', 'error']
category = ['PCS', 'TCS']

num_events = random.randint(5,25)

log_schema = {'context':[[0,99999,"patient_id|patient id number|{}"],[13,19,"patient_age|age of patient|{}"], 
[0,1,"patient_g|patient gender|{}",gender], [0,99999,"qdp_id|qualified dental provider|{}"], 
[0,10,"robot_id|robot id number|{}"]], 'events':["{}|{}|{}|{}|{}|{}"]}


for item in log_schema:
	print(item)

	if item == 'context':
		target_file = open('{}context.log'.format(args.output_type), 'w')

		for entry in log_schema[item]:
			min_val = entry[0]
			max_val = entry[1]
			try:
				if entry[3]:
					format_data = entry[3][random.randint(min_val, max_val)]
			except IndexError:
				format_data = random.randint(min_val, max_val)
			target_file.write(entry[2].format(format_data))
			target_file.write("\n")
		target_file.close()

	if item == 'events':
		print('num_events: {}'.format(num_events))
		target_file = open('{}events.log'.format(args.output_type), 'w')

		for x in range(num_events):

			s = status[random.randint(0,3)]
			c = category[random.randint(0,1)]
			event = random.randint(0,10)
			msg = 'Event {} has occured.'.format(event)
			metadata = "Yaw:{:.2f}, Pitch:{:.2f}, Roll:{:.2f}".format(random.uniform(1.0,2.0), random.uniform(-15.0,15.0), random.uniform(-2.0,2.0))


			target_file.write(log_schema[item][0].format(datetime.datetime.now(), c, s, event, msg, metadata))
			target_file.write("\n")
		target_file.close()
